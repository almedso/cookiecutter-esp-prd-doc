# {{ cookiecutter.project_name }}

{{ cookiecutter.project_short_description }}

## Features

The product documentation contains

* Requirements (including traces)
* Software architecture

is written in ReStructured Text / Sphinx.
Output can be generated as PDF or "Read The Docs" formatted HTML.

## How to use

Use the sphinx/latex toolchain docker image to build the documentation.
Run in the project directory:

```
$ docker run --rm --volume $PWD:/documents/ --user $(id -u) almedso/sphinx-doc-builder
```

The PDF document will be generated at: *./build/pdf* folder.
The html output will be generated at: *./build/html* folder. Entry point is *index.html*


## License

Copyright (c) {% now 'local', '%Y' %}, {{ cookiecutter.copyright_holder }}
{% if cookiecutter.license_identifier == 'Proprietary' -%}
All rights reserved.
{% else -%}
SPDX-License-Identifier: {{ cookiecutter.license_identifier }}
{% endif -%}

see also LICENSE file.


## Credits

This package was created with *Cookiecutter* tool and
the *ESP product documentation template*.

Cookiecutter: https://cookiecutter.readthedocs.io
ESP product documentation template: https://gitlab.com/almedso/cookiecutter-esp-prd-doc
