=====================
Firmware Requirements
=====================

.. include :: ../glossary.rst-incl

The requirements to the |firmware| component are described in this chapter.
The |firmware| requirements are derived from the product requirements.

.. only :: latex

   Summary List
   ------------

   .. item-list :: Firmware Requirements
      :filter: FW-.*


.. item :: FW-0001 Identification by name and version
   :trace:  PR-0001

   The |firmware| shall be uniquely identified by its name and a version.
   The |firmware| |image| file name shall transport the identification.
   During startup of the |firmware| the identification information shall

.. item :: FW-0002 Console
   :trace:  ESP-0001

   The |firmware| shall use an UART as a |console| channel.
   The UART shall be configured at 115200 Baud, 8 Bit, No parity, 1 Stopbit.
   The |firmware| shall operate even if there is has no consumer connected to the UART.

.. item :: FW-0003 Identification on console interface
   :trace:  PR-0001 ESP-0001

   During startup of the |firmware| the identification information shall
   be printed on the log channel.

.. item :: FW-0004 Firmware update channel
   :trace:  PR-0004 ESP-0003

   The |firmware| shall be updated via USB channel, usning the DFU protocol.
   http://www.usb.org/developers/docs/devclass_docs/DFU_1.1.pdf

   .. note ::

      This requirement must be invalidated/modified if the communication channel
      changes.

.. item :: FW-0005 Firmware update image criteria
   :trace:  ESP-0003 PR-0005

   The |product| shall only update it's |firmware| if the new |firmware| |image| is:

   * authentic: I.e. comming from a trusted source
   * integrity is given: E.g. the hash of the image is correct
   * fits to the |target|: Is made for the target board

.. item :: FW-0006 Firmware rollback
   :trace:  ESP-0003

   The |product| shall roll back to the previous |firmware| the update process fails
   or the |firmware| |image| does not meet all firmware image update criteria.



