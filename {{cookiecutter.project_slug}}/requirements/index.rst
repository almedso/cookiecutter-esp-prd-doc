============
Requirements
============

.. include :: ../glossary.rst-incl


The entire |product| is composed of multiple components.
At least there is an electronics component with the |MCU| and the |firmware|
component running on the |MCU|. Also there might be mechanical components,
optical components, chemical components and other be part part of the product.


.. only:: latex

   Table of Contents
   -----------------

   .. toctree::
      :maxdepth: 2
      :caption: Contents:

      product
      esp
      firmware

.. only:: html

   .. toctree::
      :hidden:
      :caption: Contents:

      product
      esp
      firmware



Tracability Matrix
------------------

.. item-matrix:: Product to firmware trace
   :source: FW-.*
   :target: .*
   :type: trace
