====================
Product Requirements
====================

.. include :: ../glossary.rst-incl

The requirements to the |product| are described in this chapter.


.. only:: latex

   Summary List
   ------------

   .. item-list :: Firmware Requirements
      :filter: FW-.*


.. note :: Incomplete Product Requirements

   Product requirements that initially are here are by far not complete.
   The product requirements focus on general terms such that firmware
   requirements can be derived from.

.. item :: PR-0001 Product Identification

   The product and its component are identifiable by unique name and version.

.. item :: PR-0002 Data Interface

   The product shall be able to exchange information with an
   external system via one dedicated interface.

.. item :: PR-0003 control Interface

   The product shall be able to be controlled by an
   external system via one dedicated interface.

   .. note ::

      Typical control activities are: execute firmware update, reconfigure behavior,
      request status, command some activity.

.. item :: PR-0004 Shared communication channel

   The product shall be able to multiplex the data interface and the control interface
   on one single communication channel.

   .. note ::

      The communication channel is most likely one of UART, CAN, SPI, ETH, WLAN, USB

.. item :: PR-0005 Secure communication

   The product shall support secure communication. This should be tailored by channel
   specifically. It should address privacy of information, integrity of information,
   authenticity of information.



Overview List
-------------

.. item-list:: Firmware Requirements
   :filter: PR-.*

