===========================================
Embedded Software Production - Requirements
===========================================

.. include :: ../glossary.rst-incl

The requirements to the that are required by the Embedded Software Production
process and methodology are described here. Those requirements are referenced
by the firmware if it gets to concrete bits and pieces.


.. only:: latex

   Summary List
   ------------

   .. item-list:: Embedded Software Production Requirements
      :filter: FW-.*


.. item :: ESP-0001 Firmware monitoring channel

   The |firmware| activity needs to be monitored from outside such that
   it's behavior can be observed and easily verified.

   A dedicated information channel shall be provided by the |firmware|.
   The activity information presented on that channel
   shall be human readable and also machine readable.

.. item :: ESP-0002 Hardware in a Loop Simulation

   An ESP |target| shall support the Hardware in a Loop model.
   All |MCU| peripherals that the |application| controls like
   ADC, DAC, I2C, SPI, GPIO, UART, CAN, ETH, USB shall be electronically
   accessible without any side effects.

.. item :: ESP-0003 Firmware Update

   It shall be possible to update the |firmware| over the product's
   communication channel with the outside world.
