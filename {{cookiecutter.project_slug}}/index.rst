
{% for _ in cookiecutter.project_name %}={% endfor %}
{{cookiecutter.project_name}}
{% for _ in cookiecutter.project_name %}={% endfor %}

{{ cookiecutter.project_short_description }}

License
-------

Copyright (c) {% now 'local', '%Y' %}, {{ cookiecutter.copyright_holder }}
{% if cookiecutter.license_identifier == 'Proprietary' -%}
All rights reserved.

{% else -%}
SPDX-License-Identifier: {{ cookiecutter.license_identifier }}

{% endif -%}


Table of Contents
-----------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   requirements/index
   glossary
   AUTHORS

