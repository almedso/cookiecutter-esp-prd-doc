Glossary
========

.. include :: glossary.rst-incl

.. glossary ::

   application
      is the implementation of the business logic and the business behavior
      an embedded device shall perform. It abstracts from the very specific
      hardware.

   board
      A synonym that refers to a single MCU and it's specific connected peripherals chips including those IC's that
      are fully controlled by the firmware.

      Most often this is done on a single |PCB|

   console
      is a serial interface (UART/tty) that allows basic interaction with the software of a computer system.
      In this case the computer system is a |MCU| and the |firmware| running on it.

   firmware
      Is an |application| that bound to a specific |target|.
      And therefore only runs on that specific |target|.
      Firmware is the software component and part of the product.

   image
      is a file containing the firmware that can be bulk uploaded to the |MCU|.


   MCU
      Micro Controller Unit

   PCB
      Printed Circuit Board

   product
      denominates the complete thing to create. It is composed of but is not limited to
      |firmware| component and electronic compoment, i.e. the |board|.
      The Product is tangible and delivers some value to the users of the product.

   target
      is the specific hardware where an application shall run on.
      I.e. it a |board|.
