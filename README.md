# Cookiecutter for generating ESP documentation

This is a cookiecutter template for embedded software production documentation.
It included the minimal skeleton for an empty embedded application that is

* capable to firmware update via USB interface and DFU protocol.
* has an console (UART at 115200 8N1 ) where log and version info is presented

The documentation provides a scheme for requirements and firmware architecture
as well as filled in content fitting to the startup application above

You can check the generated documentation with default values at:

https://almedso.gitlab.io/cookiecutter-esp-prd-doc


## How to use

Install cookiecutter. It requires python3 and pip(3) to be installed installed.

```
$ pip3 install  --user cookiecutter
```

Generate the project

```
$ cd directory  # one directory up where the new directory shall be created
$ cookiecutter https://gitlab.com/almedso/cookiecutter-esp-prd-doc.git
```

answer the questions the cookiecutter script is asking you.

## License

Copyright (c) 2019 almedso GmbH
SPDX-License-Identifier: MIT


## References

* https://cookiecutter.readthedocs.io/
