.PHONY: build

DOC_NAME = esp_initial_requirements_and_architecture

help:
	@echo "gen - cookiecutter without input to see result"
	@echo "build - build the generated docu"
	@echo "browse - open HTML documentation"
	@echo "view - view PDF documentation"
	@echo "clean - remove generated documentation"

gen:
	@cookiecutter --no-input .

build:
	@docker run --rm --volume $$(pwd)/$(DOC_NAME):/documents/ --user $$(id -u) almedso/sphinx-doc-builder

browse:
	@docker run --rm --volume $$(pwd):/documents/ --user $$(id -u) almedso/sphinx-doc-builder sphinx-build -W -b html $(DOC_NAME) _build/html
	firefox _build/html/index.html

view: build
	@evince $(DOC_NAME)/build/pdf/$(DOC_NAME).pdf

clean:
	@rm -rf $(DOC_NAME)
